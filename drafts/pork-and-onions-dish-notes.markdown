---
title: notes on a dish involving pork and onions
published: 2015-01-02
---

you know doro wat? it's like that but with pork (and sausage) instead of chicken

## ingredients

* lean pork, sliced into small bits
* a beer
* onions
* sausage
* fresh peppers
* bunch of garlic cloves
* spices to dump in

## what do

1. slice onions and peppers so you get long thin strips
2. pot on high heat
3. throw in onions, cover pot
4. wait a few minutes, mix up onions and throw in spices (you will probably see scorching on onions), cover again
5. 
