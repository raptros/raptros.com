+++
title = "albums in december 2014"
date = 2014-12-14
+++

here are albums i remember finding this month.

<!--more-->

Bloodshot Dawn - Demons
-----------------------
awesome shredding

- **released**: 2014
- **stream**: 
<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=3871289148/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="http://bloodshotdawn.bandcamp.com/album/demons">Demons by Bloodshot Dawn</a></iframe>

Stealing Axion - Aeons
----------------------
less djent than before.

- **released**: 2014
- **stream**:
<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=2782673096/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="http://stealingaxion.bandcamp.com/album/aeons">Aeons by Stealing Axion</a></iframe>

Intervoid - Weaponized
----------------------
cyber metal. best cyber metal i have heard in a whille

- **released**: 2014
- **stream**:
<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=987395100/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="http://intervoid.bandcamp.com/album/weaponized">Weaponized by Intervoid</a></iframe>

Woccon - Solace In Decay
------------------------
high quality death/doom

- **released**: 2014
- **stream**:
<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=2708037317/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="http://deathboundrecords.bandcamp.com/album/solace-in-decay">Solace In Decay by Woccon</a></iframe>

Noneuclid - Metatheosis
-----------------------
very strange deaththrash

- **released**: 2014
- **stream**:
<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=3526643763/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="http://blood-music.bandcamp.com/album/metatheosis">Metatheosis by Noneuclid</a></iframe>

Infestum - Monuments Of Exalted
-------------------------------
black metal with electronic stuff.

- **released**: 2014
- **stream**:
<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=1755493365/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="http://infestum.bandcamp.com/album/monuments-of-exalted">Monuments of Exalted by Infestum</a></iframe>

Anaal Nathrakh - Desideratum
----------------------------
industrial black metal? whatever they always do.

- **released**: 2014
- **stream**:
<iframe width="560" height="315" src="//www.youtube.com/embed/n2skr1tulds?rel=0" frameborder="0" allowfullscreen></iframe>

Less Than Three - The Black Box
-------------------------------
idk

- **released**: 2014
- **stream**:
<iframe width="560" height="315" src="//www.youtube.com/embed/QnrkukuMmrc?rel=0" frameborder="0" allowfullscreen></iframe>

MINDLY ROTTEN - Effacing the Origin
-----------------------------------
mind bending technical death metal.

- **released**: 2014
- **stream**:
<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=2056364810/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="http://coyoterecords.bandcamp.com/album/effacing-the-origin">Effacing The Origin by MINDLY ROTTEN</a></iframe>

Equilibrium - Erdentempel
-------------------------
some kind of folk metal. fun

- **released**: 2014
- **video**:
<iframe width="560" height="315" src="//www.youtube.com/embed/D0IS3WZC2Cs?rel=0" frameborder="0" allowfullscreen></iframe>
