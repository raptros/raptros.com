+++
title = "AGDQ 2019 runs I watched"
date = 2019-01-31
draft = true
+++

These are runs I watched most/all of (i.e. I didn't skip them). some of them
may even be good!

key:
* un-annotated: I remember this being good.
* a "?": don't remember what I thought
* other comments in parentheses.

<!-- more -->

[AGDQ 2019 playlist](https://www.youtube.com/playlist?list=PLz8YL4HVC87WJQDzVoY943URKQCsHS9XV)


kickoff
-------
* Hollow Knight
* Kerby?
* Majora's Mask?
* Portal Race?
* MGS2
* Another World
* Mega Turrican
* Ecco
* Vectorman?
* Super Ghouls n Ghosts

castlevanias
------------
* Castlevania SotN
* Castlevania 2
* Castlevania 3?

some 3d games
-------------
* Jak 3
* Splatoon 2 (sorta)
* Silent Hill 2
* nier automata (meh)
* rayman 2 (ok)

sonics
------
* Sonic Forces (no)
* Sanicball
* Sonic the Hedgehog 1

some games
----------
* Trauma Center (good but dry)
* THPS 2X (sorta good)
* Morrowind (ok)
* Michael Jackson (great)
* Puzzle Bobble
* Guacamelee 2 (decent)
* Semblance (HalfCoordinated; excellent run; ORB!)
* Warcraft 3
* TimeSplitters 2

bad games block
---------------
* Mega Man (Dos) (mixed feelings)
* Avoid the Noid
* Virtual Hydlide
* Urban Yeti
* Mohawk and Headphone Jack (puke warning)
* Star Wars Ep 1 (amazing)
* Dragon's Lair the legend (wild)
* Home Alone 2 (okj)
* Garfield (lol)
* Monkey King
* Trio the Punch

metal gear
----------
* metal gear
* metal gear 2

stuff
-----
* Overload
* Prey
* Dishonored death of the outsider

dooms
-----
* ultimate doom
* doom 2
* chex quest

more
----
* ratchet and clank UYA (great)
* GTA Vice City (great)

TAS
---
* TASBot mari0
* TASBot Castlevania: Aria of Sorrow

even more
---------
* super mario sunshine - lockout bingo race
* Final Fantasy IX (9 and a half glorious hours of memes and 90s one hit wonders)

souls
-----
* DS3 (really spectacular)
* Bloodborne (hilarious)
* Quickie World Race (kaizo)
* Super Gracie World (kaizo)
* Super Mario Odyssey (the Darker Side category makes it more interesting)
