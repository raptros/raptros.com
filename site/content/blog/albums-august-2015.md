+++
title = "albums for august 2015"
date = 2015-08-31
+++

<!--more-->

## Monolithe - Monolithe IV

* released: october 2013
* genre: funeral doom
* get: [google play](https://play.google.com/store/music/album/Monolithe_Monolithe_IV?id=Bwwj3ow2b3lyjogov5l3pdzgalm)

## Doom:VS - Earthless

* genre: death/doom
* released: may 2014
* get: [bandcamp](https://doomvs.bandcamp.com/releases)

## Harakiri for the Sky - Aokigahara

* genre: black metal
* released: april 2014
* get: [bandcamp](https://artofpropaganda.bandcamp.com/album/aokigahara)

## Veilburner - Noumenon

* genre: industrial black metal (?)
* released: (july 2015)
* get: [bandcamp](https://veilburner.bandcamp.com/album/noumenon)

## Rivers of Nihil - Monarchy

* genre: death metal
* released: august 2015
* get: [google play](https://play.google.com/store/music/album/Rivers_of_Nihil_Monarchy?id=Bzuaxcipqhzya2kdoe2wpbcllua)

## Hate Eternal - Infernus

* genre: death metal
* released: august 2015
* get: [google play](https://play.google.com/store/music/album/Hate_Eternal_Infernus?id=Bhnw4y7u6xvlb2wngkzbeceuhde)

## Soilwork - The Ride Majestic

* genre: melodic death metal
* released: august 2015
* get: [amazon](http://www.amazon.com/Ride-Majestic-Soilwork/dp/B0116UHPXM/ref=tmm_msc_swatch_0?_encoding=UTF8&qid=&sr=)

## Ahab - The Boats Of The Glen Carrig

* genre: funeral doom metal
* released: august 2015
* get: [google play](https://play.google.com/store/music/album/AHAB_The_Boats_Of_The_Glen_Carrig?id=Bq7mwncixvbhkjbsafqfvdiamp4)
