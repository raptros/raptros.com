+++
title = "information leak"
date = 2014-12-14
+++

consider the following scenario:

- you sit in front of a computer that presents to you random character sequences.
- eventually, character sequences are presented that your brain interprets as being statements about the world.
- some of these your brain will believe to be true statements, others as false.

this is information leaking into your brain.

the rate at which your brain accepts random character sequences as true factual statements may be a useful baseline for your leakiness.
