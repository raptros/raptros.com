+++
title = "albums for july 2015"
date = 2015-07-31
+++

albums

<!-- more -->

## Sunn O))) - ØØVOID 

* genre: drone doom
* released: june 2000
* get: [bandcamp](https://sunn.bandcamp.com/album/void)

## Veilburner - The Three Lightbearers 

* genre: blackened death metal
* style: industrial, technical
* released: august 2014
* get: [bandcamp](https://veilburner.bandcamp.com/album/the-three-lightbearers)

## Windfaerer - Solar

* genre: black metal
* style: atmospheric, melodic
* released: july 2012
* get: [bandcamp](https://windfaerer.bandcamp.com/album/solar)

## Amestigon - Thier

* genre: black metal
* released: may 2015
* get: [bandcamp](https://amestigon.bandcamp.com/album/thier)

## Hope Drone - Cloak of Ash

* genre: black metal/sludge
* released: july 2015
* get: [bandcam](https://hopedrone.bandcamp.com/album/cloak-of-ash)

## Ne Obliviscaris - Citadel

* genre: progressive death metal
* style: blackened, symphonic
* released: november 2014
* get: [bandcamp](https://seasonofmist.bandcamp.com/album/citadel)

## Amiensus - Ascension

* genre: atmospheric black metal
* released: july 2015
* get: [bandcamp](https://amiensus.bandcamp.com/album/ascension)

## Mare Cognitum - Phobos Monolith

* genre: ambient black metal
* released: november 2014
* get: [bandcamp](https://i-voidhangerrecords.bandcamp.com/album/phobos-monolith)
