+++
title = "albums for march 2015"
date = 2015-03-31
+++

<!--more-->

## Lascaille's Shroud - Interval 02: Parallel Infinities, The Abscinded Universe 
* released: january 2014
* genre: progressive death metal
* style: proggy
* get: [bandcamp](https://lascaillesshroud.bandcamp.com/album/interval-02-parallel-infinities-the-abscinded-universe)

## Alkaloid - The Malkuth Grimoire
* released: march 2015
* genre: progressive death metal
* style: remember what that Noneuclid album sounded like? this is the prog/tech death version of that
* get: [bandcamp](https://alkaloid-band.bandcamp.com/releases)

## Endlesshade - Wolf Will Swallow the Sun
* released: february 2015
* genre: death/doom
* get: [bandcamp](https://naturmachtproductions.bandcamp.com/album/wolf-will-swallow-the-sun-rwe009)

## Duskmourn - Legends
* released: december 2014
* genre: folk metal
* style: melodic death metal
* get: [band's site](http://www.duskmourn.com/discography/)

## Neolith - Izi.Im.Kurnu-Ki
* released: march 2015
* genre: black metal? death metal? whatever
* get: [bandcamp](https://non-serviam-records.bandcamp.com/album/neolith-izi-im-kurnu-ki)

## Exgenesis - Aphotic Veil
* released: january 2015
* genre: death/doom
* get: [bandcamp](https://naturmachtproductions.bandcamp.com/album/aphotic-veil-rwe008)

## Enslaved - In Times
* released: march 2015
* genre: progressive black metal
* get: [google](https://play.google.com/store/music/album/Enslaved_In_Times?id=Bv3hg4zmc3fwkaq7ol2acfnnriy)

## A Thousand Dead - "Chasing Goats" and "Portals"
* released: january 2015 and june 2013 respectively
* genre: instrumental prog metal
* style: groove, fusion, death metal
* get: [bandcamp/band site](http://athousanddead.com/)

## Wolfheart - Winterborn
* released: october 2013
* genre: melodic death metal
* note: re-released in 2015 by spinefarm
* get: [google](https://play.google.com/store/music/album/Wolfheart_Winterborn?id=Bgbrqiaz3z47mnj7xw6tlfrrzry)

## Aetherian - Tales of our Times
* released: january 2015
* genre: melodic death metal
* get: [bandcamp](https://aetherianband.bandcamp.com/)

## Tyranny Enthroned - Our Great Undoing
* released: november 2014
* genre: blackened death metal
* get: [bandcamp](https://blastheadrecords.bandcamp.com/album/our-great-undoing)

## Cold Cell - Generation Abomination
* released: october 2013
* genre: black metal
* get: [bandcamp](https://cold-cell.bandcamp.com/album/generation-abomination)
