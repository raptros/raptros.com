+++
title = "released wai-hmac-auth 1.0.0"
date = 2014-12-24
+++

i've released [version 1.0.0 of wai-hmac-auth][wai-hmac-auth] to hackage.  it's
a small haskell library to perform authentication of hmac-signed requests in
WAI apps; the [README][] should explain how to use it.

[wai-hmac-auth]: http://hackage.haskell.org/package/wai-hmac-auth-1.0.0
[README]: https://github.com/raptros/wai-hmac-auth/blob/master/README.md
