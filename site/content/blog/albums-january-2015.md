+++
title = "albums for january 2015"
date = 2015-02-12
+++
here are albums i found in january. only two.

<!--more-->

## Shattered Skies - *The World We Used To Know*
* released: january 2015
* genre: djent
* get: [bandcamp](https://shatteredskies.bandcamp.com/album/the-world-we-used-to-know)

## Entities - Novalis
* released: january 2015
* genre: djent
* get: [bandcamp](https://entities916.bandcamp.com/album/novalis-2)
