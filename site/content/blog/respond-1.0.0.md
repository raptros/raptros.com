+++
title = "respond 1.0.0"
date = 2014-12-15
+++

i've released verson 1.0.0 of the [respond](https://github.com/raptros/respond) library.

- there is a [guide in the README](https://github.com/raptros/respond/blob/master/README.md)
- haddock documentation is [up on Hackage](http://hackage.haskell.org/package/respond)
