+++
title = "'8÷2(2+2)' and the futile wisdom of ISO eighty thousand dash two" 
date = 2019-08-03
+++
once again, the internet has been violently reminded that the true joy of
notational standards is that there are so many to choose from! that's right,
social media is once again abuzz over an arithmetic expression using the worst
symbol, the obelus, '÷'.

neither the specific expression nor the form of it are new. arguments over
"8÷2(2+2)" can be found on forum posts at least as early as 2016, and
"48÷2(9+3)" was apparently a major controversy in 2011, having its own [Know Your
Meme](https://knowyourmeme.com/memes/48293) page.

<!-- more -->

standards
---------

the old rule of PEMDAS (parentheses, exponentiation, multiplication, division,
addition, subtraction) is a notational standard for arithmetic expressions,
defining (supposedly) a strict ordering of operator precedence. the operators
that are earlier in the list get higher precedence when parsing an expression
into a tree for evaluation, which one can think of as "getting to be closer to
the arguments".

there are of course plenty of other standards for notation, famously [Polish
Notation](https://en.wikipedia.org/wiki/Polish_notation) (a prefix notation)
and [Reverse Polish
Notation](https://en.wikipedia.org/wiki/Reverse_Polish_notation) (a postfix notation).

PEMDAS is an infix notation, but (as I hinted before) there is observably
dispute over whether this is the true standard, and whether the standard
imposes a strict precedence between multiplication and division. multiplication
and division, as is commonly known, are closely related operations; it is not
entirely unfair to dispute strict precedence between them. there is also
another wrinkle, known among people who care about programming languages as
"[operator associativity](https://en.wikipedia.org/wiki/Operator_associativity)".

operator associativity and trees
--------------------------------

operator associativity, as la wik tells us, is either rightwards or leftwards.
this tells us the tree structure that should be parsed if two operators have
the same precedence.

if (÷) and (an implicit) (\*) have the same precedence, then associativity of
(÷) tells us how the expression is actually structured. we can use parentheses
to make it explicit:
* left associative: "(8÷2)\*(2+2)", giving us 16
* right associative: "8÷(2\*(2+2))", giving us 1

it seems clear to me that preference for one answer vs the other actually comes
down to a preference for one associativity over the other. I know that I prefer
the obelus to be right associative, and therefore I prefer the second
interpretation.

preference, and getting rid of the obelus
-----------------------------------------

the issue, of course, is that there isn't a standard associativity for the
obelus. la wik, on the page for [the
obelus](https://en.wikipedia.org/wiki/Obelus) tells us 

> The ISO 80000-2 standard for mathematical notation recommends only the
> solidus or fraction bar for division, or the colon for ratios; it says that
> the obelus "should not be used" for division.

the solidus is (approximately) the "/" symbol. the fraction bar refers to the
full-on fractional representation, which the use of "/" is a shorthand for.

while I do not have the standard available to see what the ISO justification
for this rule is, the wisdom seems clear to me - the obelus has too much
ambiguity, and the alternatives do not.
        
* full fractional representation makes it extremely
  clear what other terms the division is to apply to - it effectively
  parenthesizes the elements above and below the bar.
* the solidus is generally easy to understand as being right associative (as a
  shorthand in situations where it is hard to provide a full fraction bar).
* a ratio (eg. "5:7") is understood (when recognized) as having an extremely
  high precedence, with presumably only parentheses exceeding it.

if we were to rewrite the original math then as a full on fraction, the
difference between 
```
  8
------
2(2+2)
```
and
```
8
-  * (2+2)
2
```
is obvious.

written with a fraction bar, "8/2(2+2)" should be more easily accepted as
resulting in "1". writing it out as a ratio, "8:2(2+2)" looks silly and can be
discarded.

the futility, of course, is in getting grade-school math teachers and math book
writers, especially in America, to give up on the obelus. and so we will
continue to get trolled by this wretched symbol.
