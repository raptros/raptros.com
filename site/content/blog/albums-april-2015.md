+++
title = "albums for april 2015"
date = 2015-04-12
+++

<!--more-->

## Byzantine - To Release Is To Resolve
* released: april 2015
* genre: groove metal
* style: southern, technical
* get: [bigcartel for cd](http://www.byzmerch.bigcartel.com/product/to-release-is-to-resolve), [google for digital](https://play.google.com/store/music/album/Byzantine_To_Release_Is_to_Resolve?id=Blycnaackvccs3mtp6twwdcnhqi)

## Apophys - Prime Incursion
* released: april 2015
* genre: death metal
* get: [google](https://play.google.com/store/music/album/Apophys_Prime_Incursion?id=Bt77mdpdru6wlce6ihp54cwyxhm)

## Irreversible Mechanism - Infinite Fields
* released: march 2015
* genre: death metal
* get: [bandcamp](https://blood-music.bandcamp.com/album/infinite-fields)

## The Ragnarok Prophecy - Makeria
* released: march 2015
* genre: melodic death metal
* style: technical, symphonic
* get: [bandcamp](https://theragnarokprophecy.bandcamp.com/album/makeria)

## Sulphur Aeon - Gateway to the Antisphere
* released: april 2015
* genre: death metal
* style: majestic as fuck
* get: [bandcamp](https://sulphuraeon-vanrecords.bandcamp.com/album/gateway-to-the-antisphere)

## Necrosy - Perdition
* released: april 2015
* genre: blackened death metal
* get: [stream on NCS](http://www.nocleansinging.com/2015/04/16/an-ncs-premiere-necrosy-perdition/); [buy physical](http://www.necrosy.com/necrosy-store.html); [get on google](https://play.google.com/store/music/album/Necrosy_Perdition?id=B4p4n45ribayqnoju5kt54jbivy)

## Sickening Horror - Overflow
* released: march 2015
* genre: technical death metal
* get: [bandcamp](https://deepsendrecords.bandcamp.com/album/overflow)
