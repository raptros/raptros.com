+++
title = "respond 1.1.0"
date = 2014-12-20
+++

i've released [version 1.1.0 of the respond library](http://hackage.haskell.org/package/respond-1.1.0).
it's a release meant to make it work better with monad transformers, and also
to prepare respond for a new library i'm starting on (more on that later).

from the changelog:

* add a method to MonadRespond to run an inner route with a different Request value
* implement MonadRespond instances lifted that lift over transformers
* implement MTL interface instances that lift over RespondT
* add compatibility for new version of monad-control.

